# What?

A little nim program that adds playback history records to Shellcaster/Hullcaster, when leveraging MOCP as a playback program.

![Resume Playback, Shellcaster/Hullcaster -> Mocp](.img/moctrack-resume.png)

## Why?

I manage all of my podcast on my Motorola Droid4 running Alpine, and I often find myself ssh'd into it. Because of that it's rather convenient to pop in head phones and queue up music and podcast over the already existing ssh connection. However, due to the fickle nature of technology the battery dies, I get distracted, often times thing disrupt playback. While that's not an issue for music, it's sort of disruptive to try and seek through 60-120m podcasts every time there's an issue.

For me, the perfect convergence of all of this is MOCP for music, Shellcaster/Hullcaster for podcast management, and now moctrack for history!

## Build, Configure, Install

You can build and install moctrack like so:

```
make build
sudo make install
```

And to configure shellcaster to use it, change your ~/.config/shellcaster/config.toml's playback option like so:
```
play_command = "/usr/local/bin/moctrack %s"
```
