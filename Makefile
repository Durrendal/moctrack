dep:
	nimble install db_connector

build:
	nim c -d:release src/moctrack.nim

install:
	cp src/moctrack /usr/local/bin/
