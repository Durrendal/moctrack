import std/[os, osproc, strutils]

proc mocpStatus*(): string =
  let
    status = execCmdEx("mocp -Q '%state'")
  return status.output.replace("\n", "")

proc mocpQuery*(query: string): string =
  let
    value = execCmdEx("mocp -Q " & query)
  return value.output.replace("\n", "")

proc mocpCurrentSec*(): string =
  let
    seconds = mocpQuery("%cs")
  return seconds
  
proc mocpTogglePlay*(): string =
  let
    ret = execCmdEx("mocp -G")
  return ret.output.replace("\n", "")

proc mocpPlay*(file: string): string =
  let
    ret = execProcess("mocp -l '" & file & "'")
  return ret

proc mocpSeek*(seconds: string): string =
  let
    ret = execCmdEx("mocp --seek +" & seconds)
  return ret.output.replace("\n", "")

proc mocpJump*(seconds: string): string =
  let
    ret = execCmdEx("mocp -j " & seconds & "s")
  return ret.output.replace("\n", "")

proc mocpInitServer*(): string =
  let
    ret = execCmdEx("mocp -S")
  return ret.output.replace("\n", "")

proc mocpCheckRunning*(): bool =
  let
    state = mocpStatus()
  if "FATAL_ERROR" in state:
    return false
  else:
    case state:
      of "PLAY", "PAUSE", "STOP":
        return true
