import std/[os, strutils, logging]
import db_connector/db_sqlite
import mocp

var cfg: string
var rflog: FileLogger

const histtbl = sql"""
CREATE TABLE IF NOT EXISTS history (
file STRING NOT NULL PRIMARY KEY,
cs STRING NOT NULL);
"""

proc configInit(): string =
  let
    cfgDir = getEnv("HOME") & "/.config/moctrack/"
  if dirExists(cfgDir) == false:
    createDir(cfgDir)
  return cfgDir

proc dbInit(cfg: string) =
  let
    database = cfg & "moctrack.db"
  if fileExists(database) == false:
    let
      db = open(database, "", "", "")
    defer: db.close()
    db.exec(histtbl)

#Get the playback state, file, and current second from MOCP
proc getCurrentState(): seq[string] =
  var
    query = mocpQuery("%state,%file,%cs")
  if query == "":
    query = mocpQuery("%state,%file,%cs")
    return query.split(",")
  else:
    return query.split(",")

#Get the last know current second from moctrack.db
proc getLastState(cfg: string, file: string): string =
  let
    database = cfg & "moctrack.db"
    db = open(database, "", "", "")
  defer: db.close()
  for row in db.rows(sql"SELECT cs FROM history WHERE file = ?", file):
    return row[0]

#Update an existing record with a new current second
proc putLastState(cfg: string, file: string, cs: string) =
  let
    database = cfg & "moctrack.db"
    db = open(database, "", "", "")
  defer: db.close()
  db.exec(sql"UPDATE history SET cs = ? WHERE file = ?", cs, file)

#Start a new history record
proc newHistory(cfg: string, file: string, cs: string) =
  let
    database = cfg & "moctrack.db"
    db = open(database, "", "", "")
  defer: db.close()
  db.exec(sql"INSERT INTO history (file, cs) VALUES (?, ?)", file, cs)

# Start MOCP if it isn't yet running, get the last state for a file. If it doesn't exist create a new hist record, if it does, resume the file at the last recorded point
proc beginPlayback(cfg: string, file: string) =
  let
    init = mocpCheckRunning()
  if init == false:
    discard mocpInitServer()
  let
    last = getLastState(cfg, file)
  if last != "":
    let
      lint = parseInt(last)
    rflog.log(lvlInfo, "Resuming playback @ " & last & "s")
    discard mocpPlay(file)
    if lint < 3500:
      sleep 3500
    else:
      sleep lint
    discard mocpJump(last)
    sleep 3000
  else:
    rflog.log(lvlInfo, "Starting new history.")
    newHistory(cfg, file, "0")
    discard mocpPlay(file)

# Requery MOCP every 10s for info about the playback state and record it if we're still playing, wait if w're paused, and stop if we stop playback
proc monitorPlayback() =
  while true:
    sleep 2000
    let
      state = getCurrentState()
    if state[0] == "PLAY":
      rflog.log(lvlInfo, state[0] & " " & state[1] & " " & state[2])
      putLastState(cfg, state[1], state[2])
      sleep 8000
    elif state[0] == "PAUSE":
      rflog.log(lvlInfo, state[0] & " " & state[1] & " " & state[2])
      sleep 8000
    elif state[0] == "STOP":
      rflog.log(lvlInfo, "Playback stopped, closing moctrack.")
      break

if paramCount() == 0:
  echo """Usage: moctrack /path/to/file
really this should be used from shellcaster/hullcaster the rust podcast TUI by adding the line to your config:
play_command = "moctrack %s"
"""
  quit(1)
else:
  var cfg = configInit()
  rflog = newRollingFileLogger(cfg & "track.log", fmtStr="[$time] - $levelname: ")
  addHandler(rflog)
  dbInit(cfg)
  beginPlayback(cfg, paramStr(1))
  monitorPlayback()

#Issues
# init server begins playback, but cannot see while loop info
# jump reports correct time, but neither jump nor seek actually corrects playback time.
# files with spaces in the name cause moctrack to silently crash
# this doesn't even begin to try and work for streaming, it only works on files..
